﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManaager.Entities;
using TaskManager.DAL;

namespace TaskManager.BAL
{
    public class ProjectBusiness
    {
        public List<Project> GetAllProjects()
        {
            using (TaskContext _db = new TaskContext())
            {
                return _db.Projects.ToList();
            }
        }

        public Project GetProjectByID(string id)
        {
            using (TaskContext _db = new TaskContext())
            {
                if (_db.Projects.Any(a => a.Project_ID == id))
                {
                    return _db.Projects.FirstOrDefault(a => a.Project_ID == id);
                }
                else
                {
                    throw new Exception(string.Format("Project with the id - {0} could not be found", id));
                }
            }
        }

        public bool AddProject(Project newProject)
        {
            using (TaskContext _db = new TaskContext())
            {
                _db.Projects.Add(newProject);
                _db.SaveChanges();
                return true;
            }
        }

        public bool UpdateProject(Project updateProject)
        {
            using (TaskContext _db = new TaskContext())
            {
                if (_db.Projects.Any(a => a.Project_ID == updateProject.Project_ID))
                {
                    Project projectToRemove = _db.Projects.FirstOrDefault(a => a.Project_ID == updateProject.Project_ID);
                    _db.Projects.Remove(projectToRemove);
                    _db.Projects.Add(updateProject);
                    _db.SaveChanges();
                    return true;
                }
                else
                {
                    throw new Exception(string.Format("Project with the name - {0} could not be found", updateProject.Project_Name));
                }
            }
        }


        public bool DeleteProject(string id)
        {
            using (TaskContext _db = new TaskContext())
            {
                if (_db.Projects.Any(a => a.Project_ID == id))
                {
                    Project projectToRemove = _db.Projects.FirstOrDefault(a => a.Project_ID == id);
                    _db.Projects.Remove(projectToRemove);
                    _db.SaveChanges();
                    return true;
                }
                else
                {
                    throw new Exception(string.Format("Project with the name - {0} could not be found", id));
                }
            }
        }

        public Project GetProjectFromDetails(ProjectDetails projDetails)
        {
            Project retVal = new Project();
            retVal.Project_ID = projDetails.Project_ID;
            retVal.Project_Name = projDetails.Project_Name;
            retVal.Start_Date = projDetails.Start_Date;
            retVal.End_Date = projDetails.End_Date;
            retVal.Priority = projDetails.Priority;
            retVal.User_ID = projDetails.User_ID;
            return retVal;
        }

        public List<ParentTask> GetParentTasks()
        {
            using (TaskContext _db = new TaskContext())
            {
                return _db.ParentTasks.ToList();
            }
        }

        public bool AddParentTask(ParentTask parentTask)
        {
            using (TaskContext _db = new TaskContext())
            {
                _db.ParentTasks.Add(parentTask);
                _db.SaveChanges();
                return true;
            }
        }


        public bool UpdateParentTask(ParentTask parentTask)
        {
            using (TaskContext _db = new TaskContext())
            {
                //if (_db.Projects.Any(a => a.Project_ID == updateProject.Project_ID))
                //{
                //Project projectToRemove = _db.Projects.FirstOrDefault(a => a.Project_ID == updateProject.Project_ID);
                //_db.Projects.Remove(projectToRemove);
                _db.ParentTasks.Add(parentTask);
                _db.SaveChanges();
                return true;
                //}
            }
                
            //return true;
        }

            
        }



    }

    public class ProjectDetails
    {
        public string Project_ID { get; set; }
        public string Project_Name { get; set; }
        public DateTime? Start_Date { get; set; }
        public DateTime? End_Date { get; set; }
        public int? Priority { get; set; }
        public int? NoOfTasks { get; set; }
        public int? NoOfTasksCompleted { get; set; }
        public string  User_ID { get; set; }
        //public User Manager { get; set; }
    }


