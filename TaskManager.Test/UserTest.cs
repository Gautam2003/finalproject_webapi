﻿using NUnit.Framework;
using TaskManaager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Web.Http.Results;
using TaskManager.API.Controllers;
using TaskManager.BAL;

namespace TaskManager.Test
{
    [TestFixture]
    class UserTest
    {
        [Test]
        public void Test_GetAllUsers()
        {
            UserController tc = new UserController();
            var result = tc.GetAllUsers();
            var actual = result as OkNegotiatedContentResult<List<User>>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);

        }


    }
}
