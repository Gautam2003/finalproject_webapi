﻿using NUnit.Framework;
using TaskManaager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Web.Http.Results;
using TaskManager.API.Controllers;
using TaskManager.BAL;

namespace TaskManager.Test
{
    [TestFixture]
    class ProjectTest
    {

        [Test]
        public void Test_GetAllProjects()
        {
            ProjectController tc = new ProjectController();
            var result = tc.GetAllProjects();
            var actual = result as OkNegotiatedContentResult<List<Project>>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);
            
        }

        [Test]
        public void Test_GetProjectByID()
        {
            ProjectController tc = new ProjectController();
            var result = tc.GetProjectByID("b1657300-576b-48d3-860f-2db1e794358e");
            var actual = result as OkNegotiatedContentResult<Project>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);

        }
    }
}
