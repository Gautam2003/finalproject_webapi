﻿using NUnit.Framework;
using TaskManaager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Web.Http.Results;
using TaskManager.API.Controllers;
using TaskManager.BAL;

namespace TaskManager.Test
{
    [TestFixture]
    public class TaskTest
    {
        [Test]
        public void Test_GetAllTasks()
        {
            TaskController tc = new TaskController();
            var result = tc.GetAllTasks();
            var actual = result as OkNegotiatedContentResult<List<Task>>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);



        }

        [Test]
        public void Test_GetTaskByID()
        {
            TaskController tc = new TaskController();
            var result = tc.GetAllTasks();
            var fresult = result as OkNegotiatedContentResult<List<Task>>;
            Task task = new Task();
            if (result!=null)
            {
                task = fresult.Content.FirstOrDefault();
            }
            TaskDetails taskDetails = new TaskDetails();
            taskDetails.Task_ID = task.Task_ID;
            IHttpActionResult getTaskID = tc.GetTaskByID(taskDetails.Task_ID);
            var actual = getTaskID as OkNegotiatedContentResult<TaskDetails>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);

        }


        [Test]
        public void Test_PostTask()
        {
            TaskController tc = new TaskController();
            Task taskToAdd = new Task();
            taskToAdd.TaskName = "Task from Unit Test Project";
            taskToAdd.Priority = 50;
            taskToAdd.StartDate = DateTime.Now;
            taskToAdd.EndDate = DateTime.Now.AddDays(2);

            TaskDetails taskDetails = new TaskDetails()
            {
                TaskName = "Task from Unit Test Project",
                Priority = 50,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(2)
            };
            

            //IHttpActionResult result = tc.PostTask(taskToAdd);
            IHttpActionResult result = tc.PostTask(taskDetails);
            var actual = result as OkNegotiatedContentResult<Task>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);

        }

        [Test]
        public void Test_UpdateTask()
        {
            TaskController tc = new TaskController();
            Task taskToAdd = new Task();
            taskToAdd.Task_ID = "0bf1b7e8-56c5-4a78-bc25-33d98bed7172";
            taskToAdd.TaskName = "Task updated from Unit Test Project";
            taskToAdd.Priority = 50;
            taskToAdd.StartDate = DateTime.Now;
            taskToAdd.EndDate = DateTime.Now.AddDays(5);

            TaskDetails taskDetails = new TaskDetails()
            {
                TaskName = "Task from Unit Test Project",
                Priority = 50,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(2),
                Task_ID = taskToAdd.Task_ID
            };

            IHttpActionResult result = tc.UpdateTask(taskDetails);

            //IHttpActionResult result = tc.UpdateTask(taskToAdd);
            var actual = result as OkNegotiatedContentResult<TaskDetails>;
            Assert.IsNull(actual);
            //Assert.IsNull(actual.Content);

        }

        
    }
}
