﻿using NBench;
using TaskManager.API.Controllers;
using TaskManaager.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManager.BAL;

namespace TaskManager.Performance.Test
{
    public class TaskPerformanceTest
    {
        [PerfBenchmark(Description = "Performace Test for GET", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("TestTaskCounter")]
        public void GetAllTasks_PassingTest()
        {
            TaskController controller = new TaskController();
            controller.GetAllTasks();
        }

        [PerfBenchmark(Description = "Performace Test for GET (By ID)", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("TestTaskCounter")]
        public void GetTaskByID_PassingTest()
        {
            TaskController tc = new TaskController();
            tc.GetTaskByID("0bf1b7e8-56c5-4a78-bc25-33d98bed7172");
        }

        [PerfBenchmark(Description = "Performace Test for POST", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("TestTaskCounter")]
        public void PostTask_PassingTest()
        {
            TaskController tc = new TaskController();
            Task taskToAdd = new Task()
            {
                TaskName = "Task from Unit Test Project",
                Priority = 50,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(2)
            };

            TaskDetails taskDetails = new TaskDetails()
            {
                TaskName = "Task from Unit Test Project",
                Priority = 50,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(2)
            };
            tc.PostTask(taskDetails);

        }

        //[PerfBenchmark(Description = "Performace Test for UPDATE", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        //[MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        //[ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        //[CounterMeasurement("TestTaskCounter")]
        //public void UpdateTask_PassingTest()
        //{
        //    TaskController tc = new TaskController();
        //    Task taskToAdd = new Task();
        //    taskToAdd.Task_ID = "0bf1b7e8-56c5-4a78-bc25-33d98bed7172";
        //    taskToAdd.TaskName = "Task from Unit Test Project";
        //    taskToAdd.Priority = 50;
        //    taskToAdd.StartDate = DateTime.Now;
        //    taskToAdd.EndDate = DateTime.Now.AddDays(5);

        //    TaskDetails taskDetails = new TaskDetails()
        //    {
        //        TaskName = "Task from Unit Test Project",
        //        Priority = 50,
        //        StartDate = DateTime.Now,
        //        EndDate = DateTime.Now.AddDays(2)
        //    };
        //    tc.UpdateTask(taskDetails);

        //    //tc.UpdateTask(taskToAdd);

        //}
    }
}
