﻿using System;
using System.Collections.Generic;
using TaskManaager.Entities;
using TaskManager.BAL;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TaskManager.API.Controllers
{
    [Route("api/Project")]
    public class ProjectController : ApiController
    {
        [HttpGet]
        [Route("GetAllProjects")]
        public IHttpActionResult GetAllProjects()
        {
            ProjectBusiness tb = new ProjectBusiness();
            List<Project> allProjects = tb.GetAllProjects();
            return Ok(allProjects);
        }


        [HttpGet]
        [Route("GetProjectByID")]
        public IHttpActionResult GetProjectByID(string id)
        {
            ProjectBusiness tb = new ProjectBusiness();
            Project selectedProject = tb.GetProjectByID(id);
            return Ok(selectedProject);
        }

        [HttpGet]
        [Route("GetAllProjectDetails")]
        public IHttpActionResult GetAllProjectDetails()
        {
            ProjectBusiness pb = new ProjectBusiness();
            TaskBusiness tb = new TaskBusiness();
            UserBusiness ub = new UserBusiness();
            List<ProjectDetails> retVal = new List<ProjectDetails>();

            List<Project> allProjects = new List<Project>();
            List<Task> allTasks = new List<Task>();
            List<User> allUsers = new List<User>();
            allProjects = pb.GetAllProjects();
            allTasks = tb.GetAllTasks();
            allUsers = ub.GetAllUsers();
            foreach (Project selectedProject in allProjects)
            {
                ProjectDetails pd = new ProjectDetails();
                pd.Project_ID = selectedProject.Project_ID;
                pd.Project_Name = selectedProject.Project_Name;
                pd.Start_Date = selectedProject.Start_Date;
                pd.End_Date = selectedProject.End_Date;
                pd.Priority = selectedProject.Priority;
                pd.User_ID = selectedProject.User_ID;
                if (allTasks.Any(a => a.Project_ID == selectedProject.Project_ID))
                {
                    var projectTasks = allTasks.Where(a => a.Project_ID == selectedProject.Project_ID).ToList();
                    pd.NoOfTasks = projectTasks.Count;
                    pd.NoOfTasksCompleted = projectTasks.Where(a => a.Status == "Y").Count();
                }
                /*if (allUsers.Any(a => a.Project_ID == selectedProject.Project_ID))
                {
                    pd.Manager = allUsers.Where(a => a.Project_ID == selectedProject.Project_ID).LastOrDefault();
                }*/
                retVal.Add(pd);
            }

            return Ok(retVal);
        }


        [HttpPost]
        [Route("PostProject")]
        public IHttpActionResult PostProject([FromBody] ProjectDetails projectToAdd)
        {
            ProjectBusiness tb = new ProjectBusiness();
            projectToAdd.Project_ID = Guid.NewGuid().ToString();
            tb.AddProject(tb.GetProjectFromDetails(projectToAdd));
            return Ok(projectToAdd);
        }

        [HttpPut]
        [Route("UpdateProject")]
        public IHttpActionResult UpdateProject([FromBody] ProjectDetails projectToUpdate)
        {
            ProjectBusiness tb = new ProjectBusiness();
            tb.UpdateProject(tb.GetProjectFromDetails(projectToUpdate));
            return Ok(projectToUpdate);
        }


        [HttpDelete]
        [Route("DeleteProject")]
        public IHttpActionResult DeleteProject(string id)
        {
            ProjectBusiness tb = new ProjectBusiness();
            var isDeleted = tb.DeleteProject(id);
            return Ok(isDeleted);
        }

        
        [HttpGet]
        [Route("GetAllParentTasks")]
        public IHttpActionResult GetAllParentTasks()
        {
            ProjectBusiness tb = new ProjectBusiness();
            List<ParentTask> allProjects = tb.GetParentTasks();
            return Ok(allProjects);
        }

        [HttpPost]
        [Route("PostParentTask")]
        public IHttpActionResult PostParentTask([FromBody] ParentTask parentTask)
        {
            ProjectBusiness tb = new ProjectBusiness();
            parentTask.Parent_ID = Guid.NewGuid().ToString();
            tb.AddParentTask(parentTask);
            return Ok(parentTask);
        }

        [HttpPut]
        [Route("UpdateParentTask")]
        public IHttpActionResult UpdateParentTask([FromBody] ParentTask parentTask)
        {
            ProjectBusiness tb = new ProjectBusiness();
            tb.UpdateParentTask(parentTask);
            return Ok(parentTask);
        }
    }
}

